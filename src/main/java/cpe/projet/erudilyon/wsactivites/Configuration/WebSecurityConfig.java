package cpe.projet.erudilyon.wsactivites.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import Configuration.ProfilGroup;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
                .cors().and()
                .csrf().disable()
                .authorizeRequests()

                .antMatchers("/up").permitAll()

                .antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**").permitAll()

                .anyRequest().authenticated()
                .and()
                .httpBasic();

                // Désactivation sécurité
                //.anyRequest().permitAll();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {

        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception
    {
        auth.inMemoryAuthentication()
                .withUser(ProfilGroup.ADMIN.getGroup())
                .password("{noop}"+ ProfilGroup.ADMIN.getPass())
                .roles("ADMIN");

        auth.inMemoryAuthentication()
                .withUser(ProfilGroup.USER.getGroup())
                .password("{noop}"+ ProfilGroup.USER.getPass())
                .roles("USER");

        auth.inMemoryAuthentication()
                .withUser(ProfilGroup.PARTNER.getGroup())
                .password("{noop}"+ ProfilGroup.PARTNER.getPass())
                .roles("PARTNER");
    }
}