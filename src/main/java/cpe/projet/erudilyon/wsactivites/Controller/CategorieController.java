package cpe.projet.erudilyon.wsactivites.Controller;

import Model.ActivityDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsactivites.Model.Categorie;
import cpe.projet.erudilyon.wsactivites.Service.ActivityService;
import cpe.projet.erudilyon.wsactivites.Service.CategorieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.crypto.Data;
import java.util.List;

@RestController
@Api(value = "API de gestion des Catégories")
public class CategorieController {

    @Autowired
    private CategorieService categorieService;

    @ApiOperation(value = "Permet de récupérer la totalité des categories", response = Categorie.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/categories")
    public List<Categorie> getAllCategories() {
        return categorieService.getAllCategories();
    }

    @ApiOperation(value = "Ajoute une categorie ", response = ActivityDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostMapping("/addCategorie")
    public DataValidation getAllActivities(@RequestBody Categorie categorie) {
        return categorieService.addCategorie(categorie);
    }


}
