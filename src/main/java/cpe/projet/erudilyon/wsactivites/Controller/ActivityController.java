package cpe.projet.erudilyon.wsactivites.Controller;

import Model.ActivityDTO;
import Model.CodeDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsactivites.Model.Activity;
import cpe.projet.erudilyon.wsactivites.Service.ActivityService;
import cpe.projet.erudilyon.wsactivites.Service.RestService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@CrossOrigin
@RestController
@Api(value = "API de gestion des Activités")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private RestService restService;

    @ApiOperation(value = "Permet de controler la disponibilité du service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/up")
    public String up() {
        return "UP";
    }

    @ApiOperation(value = "Récupère la liste de toutes les activités", response = ActivityDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    @GetMapping("/all")
    public List<ActivityDTO> getAllActivities() {
        return activityService.getAllActivities();
    }

    @ApiOperation(value = "Récupère la liste de toutes les activités", response = ActivityDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    @PostMapping("/allByPartner")
    public List<ActivityDTO> getAllActivitiesByPartner(@RequestBody UserDTO userDTO) {
        return activityService.getAllActivitiesByPartner(userDTO);
    }

    @ApiOperation(value = "[INTERNAL] Valider l'existence d'une activity via son code QR",
            response = DataValidation.class,
            authorizations = {@Authorization(value = "basicAuth", scopes = {@AuthorizationScope(
                    scope = " ",
                    description = " ")}) },
            notes = "Méthode Interne")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    @PostMapping("/validateByCode")
    public DataValidation validateActivityByCode(@RequestBody CodeDTO codeDTO) {

        ActivityDTO activityDTO = activityService.getActivityByCode(codeDTO);

        if(activityDTO == null)
            return new DataValidation(Activity.class.getName(), false);

        return activityService.isValidDateActivity(activityDTO);
    }

    @ApiOperation(value = "[INTERNAL] Récupère l'activity relatif au code QR",
            response = ActivityDTO.class,
            authorizations = {@Authorization(value = "basicAuth", scopes = {@AuthorizationScope(
                    scope = " ",
                    description = " ")}) },
            notes = "Méthode Interne")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    @PostMapping("/getActivityByCode")
    public ActivityDTO getActivityByCode(@RequestBody CodeDTO codeDTO) {
        return activityService.getActivityByCode(codeDTO);
    }

    @ApiOperation(value = "Ajoute une activité", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostMapping("/add")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation addActivity(@RequestBody ActivityDTO activityDTO) {

        return activityService.addActivity(activityDTO);

    }

    @ApiOperation(value = "Modifie une activité", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PutMapping("/edit")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation edit(@RequestBody ActivityDTO activityDTO) {

        activityService.editActivity(activityDTO);

        return new DataValidation(Activity.class.getName(), true);
    }

    @ApiOperation(value = "Active une activité", notes = "* Seul le champ id de l'activité est nécessaire", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PutMapping("/activate")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation activateActivity(@RequestBody ActivityDTO activityDTO) {

        return activityService.activateActivity(activityDTO);
    }

    @ApiOperation(value = "Désactive une activité", notes = "* Seul le champ id de l'activité est nécessaire", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PutMapping("/deactivate")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation deactivateActivity(@RequestBody ActivityDTO activityDTO) {

        return activityService.deactivateActivity(activityDTO);
    }

}
