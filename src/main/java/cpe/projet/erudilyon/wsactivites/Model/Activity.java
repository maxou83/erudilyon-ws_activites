package cpe.projet.erudilyon.wsactivites.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_E_ACTIVITY_ACTIVITY")
@Data
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ACTIVITY_ID")
    private Integer id;

    @Column(name = "NOM_ACTIVITY")
    private String nomActivity;

    @Column(name = "DESC_ACTIVITY", columnDefinition = "varchar(1000)")
    private String descActivity;

    @Column(name = "DATE_DEBUT_ACTIVITY")
    private Date dateDebutActivity;

    @Column(name = "DATE_FIN_ACTIVITY")
    private Date dateFinActivity;

    @Column(name = "IMG_ACTIVITY")
    private String imgActivity;

    @Column(name = "LAT_ACTIVITY")
    private String latActivity;

    @Column(name = "LONG_ACTIVITY")
    private String longActivity;

    @Column(name = "ADDRESS_ACTIVITY")
    private String adresseActivity;

    @Column(name = "GAIN_ACTIVITY")
    private BigDecimal gainActivity;

    @Column(name = "ETAT_ACTIVITY", columnDefinition = "smallint default 1")
    private Integer etatActivite;

    @Column(name = "PARTNER_ID")
    private Integer partnerActivity;

    //@Column(name = "CAT_ACTIVITY", columnDefinition = "varchar(255)")
    //@Column
    @ManyToOne
    @JoinColumn(name="CATEGORIE_ID")
    private Categorie catActivity;

}


