package cpe.projet.erudilyon.wsactivites.Model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "T_E_CATEGORIE_CATEGORIE")
@Data
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CATEGORIE_ID")
    private Integer idCategorie;

    @Column(name = "LIBELLE_CATEGORIE")
    private String libCategorie;

    @Column(name = "")

    public Integer getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Integer idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getLibCategorie() {
        return libCategorie;
    }

    public void setLibCategorie(String libCategorie) {
        this.libCategorie = libCategorie;
    }
}
