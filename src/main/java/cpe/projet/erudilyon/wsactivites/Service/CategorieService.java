package cpe.projet.erudilyon.wsactivites.Service;

import Model.ActivityDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsactivites.Model.Activity;
import cpe.projet.erudilyon.wsactivites.Model.Categorie;
import cpe.projet.erudilyon.wsactivites.Repository.ActivityRepository;
import cpe.projet.erudilyon.wsactivites.Repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategorieService {

    @Autowired
    CategorieRepository categorieRepository;

    public List<Categorie> getAllCategories(){
        List<Categorie> categorieList = new ArrayList<>();
        categorieRepository.findAll().forEach(categorieList::add);

        return categorieList;
    }

    public DataValidation addCategorie(Categorie categorie){
        categorieRepository.save(categorie);
        return new DataValidation(Categorie.class.getName(), true);
    }
}
