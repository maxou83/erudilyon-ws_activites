package cpe.projet.erudilyon.wsactivites.Service;

import Model.ActivityDTO;
import Model.CodeDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsactivites.Model.Activity;
import cpe.projet.erudilyon.wsactivites.Model.Categorie;
import cpe.projet.erudilyon.wsactivites.Repository.ActivityRepository;
import cpe.projet.erudilyon.wsactivites.Repository.CategorieRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class ActivityService {

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    CategorieRepository categorieRepository;

    @Autowired
    RestService restService;


    /**
     * Récupère toutes les activités
     * @return une liste d'activité
     */
    public List<ActivityDTO> getAllActivities(){
        List<Activity> activityList = new ArrayList<>();
        List<ActivityDTO> activityResultList = new ArrayList<>();
        activityRepository.findAll().forEach(activityList::add);
        for (Activity activity: activityList) {

            ActivityDTO activityDTO = this.transformToDTO(activity);

            activityResultList.add(activityDTO);
        }
        return activityResultList;

    }

    /**
     * Récupère toutes les activités en fonction d'un partenaire
     * @param userDTO l'objet qui contient l'ID du partenaire
     * @return une liste d'activité
     */
    public List<ActivityDTO> getAllActivitiesByPartner(UserDTO userDTO){
        List<Activity> activityList = new ArrayList<>();
        List<ActivityDTO> activityResultList = new ArrayList<>();
        activityRepository.findAllByPartnerActivity(userDTO.getUserId()).forEach(activityList::add);
        for (Activity activity: activityList) {
            ActivityDTO activityDTO = this.transformToDTO(activity);
            activityResultList.add(activityDTO);
        }
        return activityResultList;

    }

    /**
     * Récupère une seule activité grâce à un codeQR
     * @param code codeQR
     * @return une activité
     */
    public ActivityDTO getActivityByCode(CodeDTO code){
        Activity activity = activityRepository.findById(code.getActivityId()).get();
        return this.transformToDTO(activity);
    }

    /**
     * Vérification si les dates d'une activité sont valides et cohérentes
     * @param activityDTO l'activité dont il est question
     * @return un objet DataValidation indiquant false ou true à "isValid"
     */
    public DataValidation isValidDateActivity(ActivityDTO activityDTO){
        DataValidation dataValidation = new DataValidation(Activity.class.getName(), true);
        if(activityDTO.getDateDebutActivity() == null || activityDTO.getDateFinActivity() == null)
            dataValidation.setValid(false);
        else {
            Date actualDate = new Date();
            dataValidation.setValid(activityDTO.getDateDebutActivity().before(actualDate) && activityDTO.getDateFinActivity().after(actualDate));
        }

        return dataValidation;
    }


    /**
     * Ajoute une activité en base
     * @param activityDTO l'activité dont il est question
     * @return les détails du traitement, si cela a fonctionné ou non
     */
    public DataValidation addActivity(ActivityDTO activityDTO){
        return editActivity(activityDTO);
    }

    /**
     * Modifie une activité en base
     * @param activityDTO l'activité dont il est question
     * @return les détails du traitement, si cela a fonctionné ou non
     */
    public DataValidation editActivity(ActivityDTO activityDTO) {
        DataValidation dataValidation = new DataValidation();
        dataValidation.setElem(Activity.class.getName());
        try{
            activityDTO = this.setCoord(activityDTO);
            activityRepository.save(this.transformToDAO(activityDTO));
        } catch (Exception e){
            dataValidation.setStatus(e + " MANQUANT");
            dataValidation.setValid(false);
            return dataValidation;
        }
        dataValidation.setValid(true);
        return dataValidation;
    }

    /**
     * Méthode permettant d'activer une activité
     * @param activityDTO l'activité dont il est question
     * @return les détails du traitement, si cela a fonctionné ou non
     */
    public DataValidation activateActivity(ActivityDTO activityDTO) {
        DataValidation dataValidation = new DataValidation();
        dataValidation.setElem(Activity.class.getName());
        if(activityDTO.getId() == null){
            dataValidation.setValid(false);
            dataValidation.setStatus("ID MANQUANT");
            return dataValidation;
        }
        if(activityRepository.activate(activityDTO.getId()) < 1){
            dataValidation.setValid(false);
            dataValidation.setStatus("AUCUN ELEMENT AFFECTE");
            return dataValidation;
        }
        dataValidation.setValid(true);
        return dataValidation;
    }

    /**
     * Méthode permettant de désactiver une activité
     * @param activityDTO l'activité dont il est question
     * @return les détails du traitement, si cela a fonctionné ou non
     */
    public DataValidation deactivateActivity(ActivityDTO activityDTO) {
        DataValidation dataValidation = new DataValidation();
        dataValidation.setElem(Activity.class.getName());
        if(activityDTO.getId() == null){
            dataValidation.setValid(false);
            dataValidation.setStatus("ID MANQUANT");
            return dataValidation;
        }
        if(activityRepository.deactivate(activityDTO.getId()) < 1){
            dataValidation.setValid(false);
            dataValidation.setStatus("AUCUN ELEMENT AFFECTE");
            return dataValidation;
        }
        dataValidation.setValid(true);
        return dataValidation;
    }


    /**
     * Méthode de mappage qui transforme un objet Dao en objet Dto
     * @param activity l'activité telle qu'elle est en bdd
     * @return l'activité telle qu'elle est vue par les clients
     */
    private ActivityDTO transformToDTO(Activity activity){
        ActivityDTO activityDTO = new ActivityDTO();
        activityDTO.setId(activity.getId());
        activityDTO.setNomActivity(activity.getNomActivity());
        activityDTO.setDescActivity(activity.getDescActivity());
        activityDTO.setDateDebutActivity(activity.getDateDebutActivity());
        activityDTO.setDateFinActivity(activity.getDateFinActivity());
        activityDTO.setImgActivity(activity.getImgActivity());
        activityDTO.setLatActivity(activity.getLatActivity());
        activityDTO.setLongActivity(activity.getLongActivity());
        activityDTO.setAdresseActivity(activity.getAdresseActivity());
        activityDTO.setGainActivity(activity.getGainActivity());
        activityDTO.setCatActivity(activity.getCatActivity().getLibCategorie());
        activityDTO.setEtatActivity(activity.getEtatActivite());
        activityDTO.setPartnerActivity(activity.getPartnerActivity());
        return activityDTO;
    }

    /**
     * Méthode de mappage qui transforme un objet Dto en objet Dao.
     * Elle contient également tous les contrôles et vérifications
     * @param activityDTO l'activité telle qu'elle est vue par les clients
     * @return l'activité telle qu'elle est en bdd
     */
    private Activity transformToDAO(ActivityDTO activityDTO) throws Exception{
        Activity activity = new Activity();
        activity.setId(activityDTO.getId());

        if(StringUtils.isBlank(activityDTO.getNomActivity())) throw new NullPointerException("NOM");
        if(StringUtils.isBlank(activityDTO.getDescActivity())) throw new NullPointerException("DESC");
        activity.setNomActivity(activityDTO.getNomActivity());
        activity.setDescActivity(activityDTO.getDescActivity());

        /** Gestion des dates **/

        Date currentDate = new Date();

        if(activityDTO.getDateDebutActivity() == null){
            LocalDateTime localDateTimeMin = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            localDateTimeMin = localDateTimeMin.minusWeeks(1);
            Date dateMin = Date.from(localDateTimeMin.atZone(ZoneId.systemDefault()).toInstant());
            activity.setDateDebutActivity(dateMin);
        } else {
            activity.setDateDebutActivity(activityDTO.getDateDebutActivity());
        }

        if(activityDTO.getDateFinActivity() == null){
            LocalDateTime localDateTimeMax = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            localDateTimeMax = localDateTimeMax.plusMonths(2);
            Date dateMax = Date.from(localDateTimeMax.atZone(ZoneId.systemDefault()).toInstant());
            activity.setDateFinActivity(dateMax);
        } else {
            activity.setDateFinActivity(activityDTO.getDateFinActivity());
        }


        if(StringUtils.isBlank(activityDTO.getImgActivity())){
            activity.setImgActivity("https://birkeland.uib.no/wp-content/themes/bcss/images/no.png");
        } else {
            activity.setImgActivity(activityDTO.getImgActivity());
        }

        activity.setLatActivity(activityDTO.getLatActivity());
        activity.setLongActivity(activityDTO.getLongActivity());

        if(StringUtils.isBlank(activityDTO.getAdresseActivity())) throw new NullPointerException("ADRESSE");
        activity.setAdresseActivity(activityDTO.getAdresseActivity());

        if(activityDTO.getGainActivity() == null){
            activity.setGainActivity(new BigDecimal(10));
        } else {
            activity.setGainActivity(activityDTO.getGainActivity());
        }


        if(activityDTO.getEtatActivity() == null){
            activity.setEtatActivite(2);
        } else {
            activity.setEtatActivite(activityDTO.getEtatActivity());
        }

        if(StringUtils.isNotBlank(activityDTO.getCatActivity())){
            if(StringUtils.isNumeric(activityDTO.getCatActivity())){
                Integer cat = Integer.valueOf(activityDTO.getCatActivity());
                Categorie categorie =  categorieRepository.findById(cat).get();
                activity.setCatActivity(categorie);
            } else {
                Categorie categorie =  categorieRepository.findByLibCategorie(activityDTO.getCatActivity());
                activity.setCatActivity(categorie);
            }
        } else {
            throw new NullPointerException("CAT");
        }

        if(activityDTO.getPartnerActivity() == null) throw new NullPointerException("PARTNER");
        activity.setPartnerActivity(activityDTO.getPartnerActivity());

        return activity;
    }

    /**
     * Méthode qui permet de récupérer, via l'API Google Maps, les coordonnées d'une adresse passée
     * @param activityDTO l'activité dont il est question, avec l'adresse
     * @return l'activité, avec latitude et longitude "settées" aux valeurs récupérées par l'API
     */
    public ActivityDTO setCoord(ActivityDTO activityDTO){

        if(StringUtils.isNotBlank(activityDTO.getAdresseActivity())){
            if(StringUtils.isBlank(activityDTO.getLatActivity()) || StringUtils.isBlank(activityDTO.getLongActivity()) ){
                Map<String, BigDecimal> hm = restService.getLatLong(activityDTO.getAdresseActivity());
                activityDTO.setLatActivity(hm.get("lat").toString());
                activityDTO.setLongActivity(hm.get("long").toString());
            }
        }

        return activityDTO;
    }
}
