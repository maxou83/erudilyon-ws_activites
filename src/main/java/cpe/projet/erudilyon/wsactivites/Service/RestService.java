package cpe.projet.erudilyon.wsactivites.Service;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;
@Service
public class RestService {

    @Value("${googlemaps.api.key}")
    private String apiKey;

    @Value("${googlemaps.api.url}")
    private String mapsUrl;

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Map<String, BigDecimal> getLatLong(String address){

        Map<String, BigDecimal> latLong = new HashMap<String, BigDecimal>();

        try {
            URIBuilder ub = new URIBuilder(this.mapsUrl + "geocode/json");
            ub.addParameter("address", address);
            ub.addParameter("key", this.apiKey);
            String url = ub.toString();

            String returnData = restTemplate.getForObject(url,String.class);

            final JSONObject obj = new JSONObject(returnData);
            final JSONArray results = obj.getJSONArray("results");
            final JSONObject rootObj = results.getJSONObject(0);
            final JSONObject geometry = rootObj.getJSONObject("geometry");
            final JSONObject location = geometry.getJSONObject("location");

            latLong.put("lat",location.getBigDecimal("lat"));
            latLong.put("long",location.getBigDecimal("lng"));
        } catch (Exception e){
            System.out.println("Erreur : " + e);
        }

        return latLong;
    }

}
