package cpe.projet.erudilyon.wsactivites.Repository;

import cpe.projet.erudilyon.wsactivites.Model.Activity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//@Repository
public interface ActivityRepository extends CrudRepository<Activity, Integer> {

    List<Activity> findAllByPartnerActivity(Integer partner);

    Activity findActivityByNomActivity(String nomActivity);

    @Transactional
    @Modifying
    @Query("update Activity set etatActivite = 1 where id = ?1")
    int activate(Integer activityId);

    @Transactional
    @Modifying
    @Query("update Activity set etatActivite = 0 where id = ?1")
    int deactivate(Integer activityId);
}
