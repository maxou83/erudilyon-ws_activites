package cpe.projet.erudilyon.wsactivites.Repository;

import cpe.projet.erudilyon.wsactivites.Model.Categorie;
import org.springframework.data.repository.CrudRepository;

public interface CategorieRepository extends CrudRepository<Categorie, Integer> {

    Categorie findByLibCategorie(String libCategorie);
}