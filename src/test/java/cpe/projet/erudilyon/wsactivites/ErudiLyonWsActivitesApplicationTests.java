package cpe.projet.erudilyon.wsactivites;

import Model.ActivityDTO;
import Model.CodeDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsactivites.Model.Activity;
import cpe.projet.erudilyon.wsactivites.Model.Categorie;
import cpe.projet.erudilyon.wsactivites.Repository.ActivityRepository;
import cpe.projet.erudilyon.wsactivites.Repository.CategorieRepository;
import cpe.projet.erudilyon.wsactivites.Service.ActivityService;
import cpe.projet.erudilyon.wsactivites.Service.CategorieService;
import cpe.projet.erudilyon.wsactivites.Service.RestService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;


@ContextConfiguration(classes = ErudiLyonWsActivitesApplication.class)
@DataJpaTest
class ErudiLyonWsActivitesApplicationTests {

    /* -- Mock Activity -- */
    @Mock
    ActivityRepository activityRepository;

    @InjectMocks
    @Spy
    ActivityService activityService;

    @Mock
    CategorieRepository categorieRepository;

    @InjectMocks
    @Spy
    CategorieService categorieService;

    @Mock
    RestService restService;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeAll
    public static void setup() {
    }

    /**
     * Test d'intégration du service Catégorie
     */
    @Test
    public void whenCategorieServiceCountGreaterThanZero_thenOk() {

        Categorie c1 = new Categorie();
        c1.setIdCategorie(0);
        c1.setLibCategorie("Test 1");

        Categorie c2 = new Categorie();
        c1.setIdCategorie(1);
        c1.setLibCategorie("Test 2");

        List<Categorie> list = new ArrayList<Categorie>();
        list.add(c1);
        list.add(c2);

        Mockito.when(categorieRepository.findAll()).thenReturn(list);
        assertTrue(categorieService.getAllCategories().size() > 0);
    }

    /**
     * Test d'intégration du service Activity
     */
    @Test
    public void whenActivityServiceCountGreaterThanZero_thenOk() {

        Activity a1 = new Activity();
        Categorie categorie = mock(Categorie.class);
        a1.setCatActivity(categorie);

        List<Activity> list = new ArrayList<Activity>();
        list.add(a1);

        Mockito.when(activityRepository.findAll()).thenReturn(list);
        assertTrue(activityService.getAllActivities().size() > 0);
    }

    /**
     * Test d'intégration du service Activity
     */
    @Test
    public void whenActivityByPartnerServiceCountGreaterThanZero_thenOk() {

        Activity a1 = new Activity();
        Categorie categorie = mock(Categorie.class);
        a1.setCatActivity(categorie);

        UserDTO u1 = mock(UserDTO.class);

        List<Activity> list = new ArrayList<Activity>();
        list.add(a1);

        Mockito.when(activityRepository.findAllByPartnerActivity(u1.getUserId())).thenReturn(list);
        assertTrue(activityService.getAllActivitiesByPartner(u1).size() > 0);
    }

    /**
     * Test d'intégration du service Activity
     */
    @Test
    public void whenActivityByCode_NotNull_thenOk() {

        Categorie categorie = mock(Categorie.class);
        Activity a1 = new Activity();
        a1.setCatActivity(categorie);
        a1.setId(1);

        CodeDTO c1 = mock(CodeDTO.class);
        c1.setActivityId(a1.getId());

        Mockito.when(activityRepository.findById(c1.getActivityId())).thenReturn(Optional.of(a1));
        assertNotNull(activityService.getActivityByCode(c1));
    }

    /**
     * Test d'intégration d'une activité vide
     */
    @Test
    public void whenAddEmptyActivity_thenCheckFalse() {


        Activity a1 = mock(Activity.class);
        Categorie categorie = mock(Categorie.class);
        a1.setCatActivity(categorie);

        ActivityDTO a2 = mock(ActivityDTO.class);
        assertEquals(false, activityService.addActivity(a2).isValid());
    }

    /**
     * Test d'intégration d'une activité complète
     */
    @Test
    public void whenAddCompleteActivity_thenCheckValid() {

        ActivityDTO a1 = new ActivityDTO();
        a1.setNomActivity("TEST");
        a1.setDescActivity("TEST");
        a1.setGainActivity(new BigDecimal(50));
        a1.setDateDebutActivity(new Date());
        a1.setDateFinActivity(new Date());
        a1.setAdresseActivity("2 Rue Jacquard, 69120 Vaulx-en-Velin");
        a1.setPartnerActivity(1);
        a1.setEtatActivity(2);
        a1.setImgActivity("TEST");

        Map<String, BigDecimal> hm2 = new HashMap<>();
        hm2.put("lat", new BigDecimal(5));
        hm2.put("long", new BigDecimal(5));
        Mockito.when(restService.getLatLong(a1.getAdresseActivity())).thenReturn(hm2);

        a1.setCatActivity("TEST");

        DataValidation d = activityService.addActivity(a1);
        assertEquals(true, d.isValid());
    }


    /**
     * Test d'intégration d'une activité sans nom
     *
     * @result Le status doit montrer une erreur
     */
    @Test
    public void whenAddActivityWithoutName_thenCheckStatus() {

        ActivityDTO a1 = new ActivityDTO();
        /* OMISSION DU NOM */
        a1.setDescActivity("TEST");
        a1.setGainActivity(new BigDecimal(50));
        a1.setDateDebutActivity(new Date());
        a1.setDateFinActivity(new Date());
        a1.setAdresseActivity("2 Rue Jacquard, 69120 Vaulx-en-Velin");
        a1.setPartnerActivity(1);
        a1.setEtatActivity(2);
        a1.setImgActivity("TEST");

        Map<String, BigDecimal> hm2 = new HashMap<>();
        hm2.put("lat", new BigDecimal(5));
        hm2.put("long", new BigDecimal(5));
        Mockito.when(restService.getLatLong(a1.getAdresseActivity())).thenReturn(hm2);

        a1.setCatActivity("TEST");

        DataValidation d = activityService.addActivity(a1);
        //System.out.println(d.getStatus());
        assertEquals("java.lang.NullPointerException: NOM MANQUANT", d.getStatus());
    }

    /**
     * Test d'intégration d'une activité sans description
     *
     * @result Le status doit montrer une erreur
     */
    @Test
    public void whenAddActivityWithoutDesc_thenCheckStatus() {

        ActivityDTO a1 = new ActivityDTO();
        a1.setNomActivity("TEST");
        /* OMISSION DE LA DESCRIPTION */
        a1.setGainActivity(new BigDecimal(50));
        a1.setDateDebutActivity(new Date());
        a1.setDateFinActivity(new Date());
        a1.setAdresseActivity("2 Rue Jacquard, 69120 Vaulx-en-Velin");
        a1.setPartnerActivity(1);
        a1.setEtatActivity(2);
        a1.setImgActivity("TEST");

        Map<String, BigDecimal> hm2 = new HashMap<>();
        hm2.put("lat", new BigDecimal(5));
        hm2.put("long", new BigDecimal(5));
        Mockito.when(restService.getLatLong(a1.getAdresseActivity())).thenReturn(hm2);

        a1.setCatActivity("TEST");

        DataValidation d = activityService.addActivity(a1);
        assertEquals("java.lang.NullPointerException: DESC MANQUANT", d.getStatus());
    }


    /**
     * Test d'intégration d'une activité sans ID de partenaire
     *
     * @result Le status doit montrer une erreur
     */
    @Test
    public void whenAddActivityWithoutPartner_thenCheckStatus() {

        ActivityDTO a1 = new ActivityDTO();
        a1.setNomActivity("TEST");
        a1.setDescActivity("TEST");
        a1.setGainActivity(new BigDecimal(50));
        a1.setDateDebutActivity(new Date());
        a1.setDateFinActivity(new Date());
        a1.setAdresseActivity("2 Rue Jacquard, 69120 Vaulx-en-Velin");
        /* a1.setPartnerActivity(1); */
        a1.setEtatActivity(2);
        a1.setImgActivity("TEST");

        Map<String, BigDecimal> hm2 = new HashMap<>();
        hm2.put("lat", new BigDecimal(5));
        hm2.put("long", new BigDecimal(5));
        Mockito.when(restService.getLatLong(a1.getAdresseActivity())).thenReturn(hm2);

        a1.setCatActivity("TEST");

        DataValidation d = activityService.addActivity(a1);
        assertEquals("java.lang.NullPointerException: PARTNER MANQUANT", d.getStatus());
    }

    /**
     * Test d'intégration d'une activité sans date ni image
     *
     * @result La méthode doit être valide
     */
    @Test
    public void whenAddActivityWithoutDateAndImage_thenCheckValid() {

        ActivityDTO a1 = new ActivityDTO();
        a1.setNomActivity("TEST");
        a1.setDescActivity("TEST");
        a1.setGainActivity(new BigDecimal(50));
        /*a1.setDateDebutActivity(new Date());
        a1.setDateFinActivity(new Date());*/
        a1.setAdresseActivity("2 Rue Jacquard, 69120 Vaulx-en-Velin");
        a1.setPartnerActivity(1);
        a1.setEtatActivity(2);
        /*a1.setImgActivity("TEST");*/

        Map<String, BigDecimal> hm2 = new HashMap<>();
        hm2.put("lat", new BigDecimal(5));
        hm2.put("long", new BigDecimal(5));
        Mockito.when(restService.getLatLong(a1.getAdresseActivity())).thenReturn(hm2);

        a1.setCatActivity("TEST");

        DataValidation d = activityService.addActivity(a1);
        assertEquals(true, d.isValid());
    }


}
