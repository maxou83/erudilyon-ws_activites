FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/ws-activites-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8083
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]